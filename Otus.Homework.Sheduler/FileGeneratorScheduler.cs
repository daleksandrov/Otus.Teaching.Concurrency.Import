﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Otus.Homework.Sheduler
{
    public class FileGeneratorScheduler : IFileGeneratorScheduler
    {
        private string _handlerProcessFileName = String.Empty;
        private string _handlerProcessDirectory = String.Empty;

        public void FileGenerate(IConfiguration configuration)
        {
            this._handlerProcessFileName = configuration["HandlerProcessFileName"];
            this._handlerProcessDirectory = configuration["HandlerProcessDirectory"];
            
            var stopWatch = new Stopwatch();
            
            Console.WriteLine("Process scheduler...");
            Console.WriteLine("Generate File...");
            stopWatch.Start();

            Process process = StartHandlerProcess();

            process.WaitForExit();
            
            stopWatch.Stop();
            
            Console.WriteLine($"Generated File in {stopWatch.Elapsed}...");
        }

        private Process StartHandlerProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                Arguments = "customers",
                FileName = GetPathToHandlerProcess(),
            };
            
            var process = Process.Start(startInfo);

            return process;
        }

        private string GetPathToHandlerProcess()
        {
            return Path.Combine(_handlerProcessDirectory, _handlerProcessFileName);
        }
    }
}