﻿using Microsoft.Extensions.Configuration;

namespace Otus.Homework.Sheduler
{
    public interface IFileGeneratorScheduler
    {
        void FileGenerate(IConfiguration configuration);
    }
}