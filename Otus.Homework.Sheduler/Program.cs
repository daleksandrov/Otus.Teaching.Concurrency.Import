﻿using System;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Homework.Sheduler
{
    class Program
    {
        private static IConfiguration configuration;
        
        private static string _dataFileName = String.Empty;
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _dataFileName);

        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            _dataFileName = configuration["DefaultFileName"];

            Console.WriteLine($"Scheduler started with process Id {Process.GetCurrentProcess().Id}...");

            if (args != null && args.Length > 0 && args[0].Equals("method"))
            {
                Console.WriteLine("Generating xml data...");

                GenerateDataFile();
            
                Console.WriteLine($"Generated xml data in {_dataFileName}...");
            }
            else
            {
                var scheduler = new FileGeneratorScheduler();
                scheduler.FileGenerate(configuration);
            }
        }
        
        static void GenerateDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000000);
            xmlGenerator.Generate();
        }
        
        
    }
}