using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    public interface IDataGenerator
    {
        void Generate();

        Task GenerateAsync();
    }
}