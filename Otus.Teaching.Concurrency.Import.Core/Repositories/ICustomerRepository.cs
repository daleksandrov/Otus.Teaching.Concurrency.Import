using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void Save();
        
        ValueTask AddCustomerAsync(Customer customer);
        Task SaveAsync();

        Task AddCustomerManualAsync(Customer customer);
        Task SaveManualAsunc();
    }
}