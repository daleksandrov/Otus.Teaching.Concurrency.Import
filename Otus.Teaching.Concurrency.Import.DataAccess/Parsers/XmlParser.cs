﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private string _dataFilePath;

        public XmlParser(string dataFilePath)
        {
            _dataFilePath = dataFilePath;
        }

        public List<Customer> Parse()
        {
            using var stream = File.OpenRead(_dataFilePath);
            var deserializer = new XmlSerializer(typeof(CustomersList));
            var customers = deserializer.Deserialize(stream);
            return ((CustomersList)customers).Customers;
        }
    }
}