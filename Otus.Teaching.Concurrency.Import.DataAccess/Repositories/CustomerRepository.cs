using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly DataContext _dataContext;
        public CustomerRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        /// <summary>
        /// Добавление Customer в базу (синхронно)
        /// </summary>
        /// <param name="customer"></param>
        public void AddCustomer(Customer customer)
        {
            //Add customer to data source
            _dataContext.Customers.Add(customer);
        }
        
        /// <summary>
        /// Сохранение изменений в БД (синхронно)
        /// </summary>
        public void Save()
        {
            _dataContext.SaveChanges();
        }
        
        /// <summary>
        /// Добавление Customer в базу (используется асинхронный метод адаптера БД)
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public async ValueTask AddCustomerAsync(Customer customer)
        {
            await _dataContext.Customers.AddAsync(customer);
        }
        
        /// <summary>
        /// СОхранение изменений в БД (используется асинхронный метод адаптера БД)
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            await   _dataContext.SaveChangesAsync();
        }

        public async Task AddCustomerManualAsync(Customer customer)
        {
            await Task.Run(() => AddCustomer(customer));
        }

        public async Task SaveManualAsunc()
        {
            await Task.Run(() => Save());
        }

    }
}