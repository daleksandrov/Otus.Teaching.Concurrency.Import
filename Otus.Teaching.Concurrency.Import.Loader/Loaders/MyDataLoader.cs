﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class MyDataLoader : IDataLoader
    {
        private readonly List<Thread> _loadFileHandlerThreads = new List<Thread>();
        private Dictionary<int,int> _repeatAttempt = new Dictionary<int, int>();
        private readonly int _maxCountRepeat;
        private List<Customer> _customerList;
        private int _countThreads;

        public MyDataLoader(List<Customer> customerList, int countThreads, int maxCountRepeat)
        {
            _countThreads = countThreads;
            _customerList = customerList;
            _maxCountRepeat = maxCountRepeat;
        }

        /// <summary>
        /// Разделение списка на части и создание обработчика в отдельных потоках
        /// </summary>
        public void LoadData()
        {
            Console.WriteLine("Loading data...");
            
            var stopWatch = new Stopwatch();
            
            stopWatch.Start();

            int range = _customerList.Max(x=>x.Id) / _countThreads;
            int offset = 0;

            for (var i = 0; i < _countThreads; i++)
            {
                var customersSubList = _customerList.Where(x => x.Id > offset && x.Id <= range).ToList();
                if (customersSubList != null && customersSubList.Count > 0)
                    _loadFileHandlerThreads.Add(StartHandlerThread(customersSubList));
                offset = range;
                range += range;
            }

            //Чтобы подсчитать общее время, дожидаемся окончания работы всех потоков
            foreach (var handlerThread in _loadFileHandlerThreads)
            {
                handlerThread.Join();
            }
            
            stopWatch.Stop();
            
            Console.WriteLine($"Loaded data in {stopWatch.Elapsed}...");
        }

        /// <summary>
        /// Запуск обработчика 
        /// </summary>
        /// <param name="customersSubList"></param>
        /// <returns></returns>
        private Thread StartHandlerThread(List<Customer> customersSubList)
        {
            
            var thread = new Thread(() => { ProcessSubList(customersSubList); });

            //Инициализируем для потока счетчик повторов
            _repeatAttempt.Add(thread.ManagedThreadId, _maxCountRepeat);
            
            thread.Start();

            return thread;
        }

        
        /// <summary>
        /// Обработка списка 
        /// </summary>
        /// <param name="customersSubList"></param>
        /// <exception cref="Exception"></exception>
        private  void ProcessSubList(List<Customer> customersSubList)
        {
            try
            {
                using DataContext dataContext = new DataContext();
                ICustomerRepository customerRepository = new CustomerRepository(dataContext);
                
                foreach (var custom in customersSubList)
                {
                    customerRepository.AddCustomerManualAsync(custom).ConfigureAwait(false);
                }

                customerRepository.SaveManualAsunc().ConfigureAwait(false);
                
                Console.WriteLine(
                    $"Current thread {Thread.CurrentThread.ManagedThreadId}, Customers Added: {customersSubList.Count}");
            }
            catch (Exception e)
            {
                //Вызываем метод рекурсивно, пока счетчик исключений > 0
                int countRepeat = _repeatAttempt[Thread.CurrentThread.ManagedThreadId]--;
                
                if (countRepeat > 0)
                {
                    Console.WriteLine($"Произошло исключение в потоке: {Thread.CurrentThread.ManagedThreadId} - {e.Message} Осталось {countRepeat} повторов");
                    ProcessSubList(customersSubList);
                }
            }
        }
    }
}