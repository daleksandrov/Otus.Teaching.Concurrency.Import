﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static IConfiguration configuration;
        
        private static string _dataFilePath;
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            else
            {
                configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configuration["DefaultFileName"]);
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();

            var customerList = new XmlParser(_dataFilePath).Parse();

            using DataContext dataContext = new DataContext();
            DbInitializer dbInitializer = new DbInitializer(dataContext);
            dbInitializer.Initialize();
            
            var loader = new MyDataLoader( customerList,5, 5);

            loader.LoadData();
        }


        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1_000_000);
            //xmlGenerator.Generate(); 
            xmlGenerator.GenerateAsync().Wait();
        }
    }
}